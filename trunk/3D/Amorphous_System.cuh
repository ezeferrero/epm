#include "Amorphous_System_kernels_2.cuh"

using namespace std;
using namespace thrust::placeholders;

//////===========================================================//////
//////========= DEFINING THE AMORPHOUS SYSTEM CLASS =============//////
//////===========================================================//////

class Amorphous_System{

	public:
	
	/////////////////////////////////////////////////////////////////////////////////
	//////----- Physical Quantities and Intermediate Computing Quantities -----//////
	/////////////////////////////////////////////////////////////////////////////////
	cufftDoubleReal *d_sigma;
	double *d_sigma_onedtbefore;
	double *d_sigma_yield;
	bool *d_state;
	bool *d_state_backup;
	cufftDoubleReal *d_conv_result;
	cufftDoubleReal *d_pl_shearate;
	cufftDoubleComplex *d_conv_result_TF;
	cufftDoubleComplex *d_pl_shearate_TF;
	cufftDoubleReal *d_gamma_plastic;
	cufftDoubleComplex *d_G_TF;
	cufftDoubleComplex *h_G_TF;// = new cufftDoubleComplex[N_TF];
	
	///////////////////////////////////////////////
	//////----- Fourier-Transform Plans -----//////
	///////////////////////////////////////////////
	cufftHandle P_r2c;
	cufftHandle P_c2r;

	//////////////////////////////////////////
	//////----- Pre-Initialization -----//////
	////////////////////////////////////////// 
	Amorphous_System(){
	
		cudaMalloc((void**) &d_sigma , sizeof(cufftDoubleReal)*N);
		cudaMalloc((void**) &d_sigma_onedtbefore , sizeof(double)*N);
		cudaMalloc((void**) &d_sigma_yield, sizeof(double)*N);
		cudaMalloc((void**) &d_state , sizeof(bool)*N);
		cudaMalloc((void**) &d_state_backup, sizeof(bool)*N);
		cudaMalloc((void**) &d_conv_result , sizeof(cufftDoubleReal)*N);
		cudaMalloc((void**) &d_pl_shearate , sizeof(cufftDoubleReal)*N);
		cudaMalloc((void**) &d_conv_result_TF , sizeof(cufftDoubleComplex)*N_TF);
		cudaMalloc((void**) &d_pl_shearate_TF , sizeof(cufftDoubleComplex)*N_TF);
		cudaMalloc((void**) &d_gamma_plastic, sizeof(cufftDoubleReal)*N);	
		cudaMalloc((void**) &d_G_TF, sizeof(cufftDoubleComplex)*N_TF);
		//h_G_TF = (cufftDoubleComplex *)malloc(N_TF*sizeof(cufftDoubleComplex));
		h_G_TF = new cufftDoubleComplex[N_TF]; 

		cudaMemset(d_sigma , 0, sizeof(cufftDoubleReal)*N);
		cudaMemset(d_sigma_onedtbefore, 0, sizeof(double)*N);
		cudaMemset(d_sigma_yield , 0, sizeof(double)*N);
		cudaMemset(d_state , 0, sizeof(bool)*N);
		cudaMemset(d_state_backup, 0, sizeof(bool)*N);
		cudaMemset(d_conv_result , 0, sizeof(cufftDoubleReal)*N);
		cudaMemset(d_pl_shearate , 0, sizeof(cufftDoubleReal)*N);
		cudaMemset(d_conv_result_TF , 0, sizeof(cufftDoubleComplex)*N_TF);
		cudaMemset(d_pl_shearate_TF , 0, sizeof(cufftDoubleComplex)*N_TF);
		cudaMemset(d_gamma_plastic, 0, sizeof(cufftDoubleReal)*N);
		cudaMemset(d_G_TF, 0, sizeof(cufftDoubleComplex)*N_TF);
		
		cufftPlan3d(&P_r2c , Lx, Ly, Lz, CUFFT_D2Z);
		cufftPlan3d(&P_c2r , Lx, Ly, Lz, CUFFT_Z2D);	

	}
	
	////////////////////////////////////////////////
	//////----- Defining Propagator In 3D-----//////
	////////////////////////////////////////////////
	void Propagator_Initialization(){
	
		double qx ,qy ,qz, modul2, modul4;

		string pro_c="propagator_complex.dat";	
		ofstream propagator_complex(pro_c.c_str());

        	for(int i=0;i<Lx;i++){
        	        for(int j=0;j<Ly;j++){
        	                for(int k=0;k<Lz/2+1;k++){
        	                        if(i <= Lx/2 ){
        	                                qx = 2.*M_PI*double(i)/double(Lx);
        	                        }
        	                        else{
        	                                qx = 2.*M_PI*double(i-Lx)/double(Lx);
        	                        }
	
        	                        if(j <= Ly/2 ){
        	                                qy = 2.*M_PI*double(j)/double(Ly);
        	                        }
        	                        else{
        	                                qy = 2.*M_PI*double(j-Ly)/double(Ly);
        	                        }
					qz = 2.*M_PI*double(k)/double(Lz);
	
        	                        modul2 = qx*qx + qy*qy + qz*qz ;
        	                        modul4 = modul2*modul2 ;
        	
		                        if(modul2 == 0.){
        	                                h_G_TF[i*Ly*(Lz/2+1)+j*(Lz/2+1)+k].x = -1.;
						h_G_TF[i*Ly*(Lz/2+1)+j*(Lz/2+1)+k].y = 0. ;
       		                        }
	                                else{
	                                        h_G_TF[i*Ly*(Lz/2+1)+j*(Lz/2+1)+k].x = -1.*(4.*qx*qx*qy*qy + qz*qz*modul2)/modul4;
	                                        h_G_TF[i*Ly*(Lz/2+1)+j*(Lz/2+1)+k].y = 0. ;
	                                }
	                		
					propagator_complex<<h_G_TF[i*Ly*(Lz/2+1)+j*(Lz/2+1)+k].x<<endl;
			        }
	                }
	        }
		/// Copy the propagator to the Device :
		cudaMemcpy(d_G_TF, h_G_TF, sizeof(cufftDoubleComplex)*N_TF, cudaMemcpyHostToDevice);				
	}

	//////////////////////////////////////////////////
	//////----- Two Initialization Options -----//////
	//////////////////////////////////////////////////
		
	/// --- Option 1. ---///	
	void InitializationZero(){
		cout<<" >> Initial-Conditions Zero !"<<endl;
		cudaMemset(d_sigma , 1., sizeof(cufftDoubleReal)*N);
		cudaMemset(d_sigma_onedtbefore, 0, sizeof(double)*N);
		cudaMemset(d_sigma_yield , 0, sizeof(double)*N);
		cudaMemset(d_state , 0, sizeof(bool)*N);
		cudaMemset(d_state_backup, 0, sizeof(bool)*N);
		cudaMemset(d_conv_result , 0, sizeof(cufftDoubleReal)*N);
		cudaMemset(d_pl_shearate , 0, sizeof(cufftDoubleReal)*N);
		cudaMemset(d_conv_result_TF , 0, sizeof(cufftDoubleComplex)*N_TF);
		cudaMemset(d_pl_shearate_TF , 0, sizeof(cufftDoubleComplex)*N_TF);
		cudaMemset(d_gamma_plastic, 0, sizeof(cufftDoubleReal)*N);
		cout<<" >> Initial-Conditions Zero Success !"<<endl; 
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//////----- Constant Driven Experiment : System's Response To A Imposed Shear Rate -----////// 
	//////////////////////////////////////////////////////////////////////////////////////////////
	double ShearrateFix_Response_Simulation(double gamma_p, double Tmax, double THE_EXPONENT){
		
		///==================================================================================================///	
		///--- Preparing The Files Saving The Mesurement Results of Simulation and Corresponding Variables---///
		///==================================================================================================///				
			//prepare the folder name for saving all files	
			char* dir;
			dir = folder_name_sim_id_producer();
			string str_dir = dir; //: convert a char to string

			std::stringstream shear_rate_expo;
			shear_rate_expo<<std::fixed;
			shear_rate_expo<<std::setprecision(1);
        		shear_rate_expo<<THE_EXPONENT;
        		string name_shear_rate_expo = shear_rate_expo.str();

                        cout << "PRINT THYM"<<endl;

			//--------------------------------------------------------//
			/////--Instaneous Everage Over all sites (over space)--/////
			//--------------------------------------------------------//	
			//: Stress ://		
			string name_IEOS_STRESS = str_dir + "/IEOS_STRESS_"+name_shear_rate_expo+".dat";
			ofstream file_IEOS_STRESS(name_IEOS_STRESS.c_str());
			file_IEOS_STRESS << std::fixed;
                	file_IEOS_STRESS << std::setprecision(9);
			double ieos_stress = 0.;
			
			//: State ://
			string name_IEOS_STATE = str_dir + "/IEOS_STATE_"+name_shear_rate_expo+".dat";
			ofstream file_IEOS_STATE(name_IEOS_STATE.c_str());
			file_IEOS_STATE << std::fixed;
                	file_IEOS_STATE << std::setprecision(9);
			
			cufftDoubleReal *d_state_double;    ////: also used in the one dimensional average calculation !!
			cudaMalloc((void**) &d_state_double , sizeof(cufftDoubleReal)*N);
			cudaMemset(d_state_double, 0, sizeof(cufftDoubleReal)*N);		
			double ieos_state = 0.;

			//: VELOCITY OF STRESS ://
			string name_IEOS_VELOCITY_STRESS = str_dir + "/IEOS_VELOCITY_STRESS_"+name_shear_rate_expo+".dat";
			ofstream file_IEOS_VELOCITY_STRESS(name_IEOS_VELOCITY_STRESS.c_str());
			file_IEOS_VELOCITY_STRESS << std::fixed;
                	file_IEOS_VELOCITY_STRESS << std::setprecision(9);
			double ieos_stress_now = 0;
			double ieos_stress_onedtbefore = 0;
			double ieos_stress_velocity = 0;
			
			//: STRESS-DROP RATE ://
			string name_IEOS_SDR = str_dir + "/IEOS_SDR_"+name_shear_rate_expo+".dat";
			ofstream file_IEOS_SDR(name_IEOS_SDR.c_str());
			file_IEOS_SDR << std::fixed;
                	file_IEOS_SDR << std::setprecision(9);
			double ieos_sdr = 0;
			
			//------------------------------------------------------------------------------------//
			/////--Local Temporal Correlation Function (everaged over space-sites) Of Stress--//////
			//------------------------------------------------------------------------------------//
			string name_LTCF_STRESS = str_dir + "/LTCF_STRESS_" + name_shear_rate_expo + ".dat";
			ofstream file_LTCF_STRESS(name_LTCF_STRESS.c_str()); 
			file_LTCF_STRESS << std::fixed;
			file_LTCF_STRESS << std::setprecision(9);

			double *d_sigma_w;
			cudaMalloc((void**) &d_sigma_w, sizeof(double)*N);
			cudaMemset(d_sigma_w, 0 , sizeof(double)*N);

			double *d_sigma_w_2; //For saving the power two of stress at t_w  = sigma_w^2 , this is for normalization
			cudaMalloc((void**) &d_sigma_w_2, sizeof(double)*N);
			cudaMemset(d_sigma_w_2, 0 , sizeof(double)*N);

			double *correlation;	
			cudaMalloc((void**) &correlation, sizeof(double)*N);
			cudaMemset(correlation, 0 , sizeof(double)*N);
			
			double space_aver_produit;	
			double correlation_space_aver;
			double space_aver_sigma_w;
			double space_aver_sigma_w_2;
			double fluctuation_w;

			//------------------------------------------------//				
			/////---Instaneous Physical Quantities Field---/////
			//------------------------------------------------//
			//: Stress ://
			string name_IPQF_STRESS = str_dir + "/IPQF_STRESS_" + name_shear_rate_expo + ".dat";
			ofstream file_IPQF_STRESS(name_IPQF_STRESS.c_str());
			file_IPQF_STRESS << std::fixed;
			file_IPQF_STRESS << std::setprecision(9);
			double *h_sigma = new double[N];
			//: State ://
			string name_IPQF_STATE = str_dir + "/IPQF_STATE_" + name_shear_rate_expo + ".dat";
			ofstream file_IPQF_STATE(name_IPQF_STATE.c_str());
			file_IPQF_STATE << std::fixed;
			file_IPQF_STATE << std::setprecision(9);
			bool *h_state = new bool[N];
			int counter_ipqf_state = 0;
			//: Accumulated Active State ://
			string name_IPQF_ACCUM_ACTIVE_STATE = str_dir + "/IPQF_ACCUM_ACTIVE_STATE_" + name_shear_rate_expo + ".dat";
			ofstream file_IPQF_ACCUM_ACTIVE_STATE(name_IPQF_ACCUM_ACTIVE_STATE.c_str());
			file_IPQF_ACCUM_ACTIVE_STATE << std::fixed ;
			file_IPQF_ACCUM_ACTIVE_STATE << std::setprecision(9);
			double *h_accum_active_state = new double[N];
			double *d_accum_active_state;
			cudaMalloc((void**) &d_accum_active_state, sizeof(double)*N);
			cudaMemset(d_accum_active_state, 0, sizeof(double)*N);

			//------------------------------------------------------------------------------------//				
			/////---Instaneous Local Random-Barrier : Distribution and Distance-Distribution---/////
			//------------------------------------------------------------------------------------//
			//: Random-Barrier Distribution ://
			string name_ILRB_RBD = str_dir + "/ILRB_RBD_" + name_shear_rate_expo + ".dat";
			ofstream file_ILRB_RBD(name_ILRB_RBD.c_str());
			file_ILRB_RBD << std::fixed;
			file_ILRB_RBD << std::setprecision(9);
			double *h_sigma_yield_Distrbt = new double[N];
			double *d_sigma_yield_Distrbt;
			cudaMalloc((void**) &d_sigma_yield_Distrbt, sizeof(double)*N);
			cudaMemset(d_sigma_yield_Distrbt, 0, sizeof(double)*N);
			//: Distance-Distribution ://
			string name_ILRB_DD = str_dir + "/ILRB_DD_" + name_shear_rate_expo + ".dat";
			ofstream file_ILRB_DD(name_ILRB_DD.c_str());
			file_ILRB_DD << std::fixed ;
			file_ILRB_DD << std::setprecision(9);
			double *h_distance_Distrbt = new double[N];
			double *d_distance_Distrbt;
			cudaMalloc((void**) &d_distance_Distrbt, sizeof(double)*N);
			cudaMemset(d_distance_Distrbt, 0, sizeof(double)*N);

			//-----------------------------------------------------------------------------------------------------------------------//				
			/////---Instaneous Population of certain (sigma, state): list of (sigma, state) for all sites on squence of moments---/////
			//-----------------------------------------------------------------------------------------------------------------------//
			string name_IPSS = str_dir + "/IPSS_" + name_shear_rate_expo + ".dat";
			ofstream file_IPSS(name_IPSS.c_str());
			file_IPSS << std::fixed ;
			file_IPSS << std::setprecision(9);
			int counter_ipss = 0;
			//double *h_IPSS_sigma = new double[N];
			//double *h_IPSS_state = new double[N];  // a host memory created for sigma and state in ipqf can also be used
			
			//----------------------//				
			/////---Flow Curve---/////
			//----------------------//
			double stress_st = 0;
			int fc_counter = 0;

			//------------- Report Success ------------------//
			cout<<" >> ShearrateFix_Response_Simulation : Preparing The Files Saving The Mesurement Results of Simulation and Corresponding Variables >>> Success ! "<<endl;

		///=======================================///
		///--- For GPU - Paralization Settings ---///
		///=======================================///
		dim3 grid(Lx/TILE_X, Ly/TILE_Y, Lz/TILE_Z);
		dim3 block(TILE_X, TILE_Y, TILE_Z);
		dim3 grid_convol(Lx/TILE_X,Ly/TILE_Y,Lz/TILE_Z/2+1);

		///==========================================///
		///--- For Philox-Random-Number-Generator ---///
		///==========================================///
		unsigned long time_counter = 23;		

		///=================================================///
		///--- Setting Simulation Chronometer And Stepper---///
		///=================================================///
		double t = 0.;
		int step = 0;
		
		///=========================================================///
		///--- Initializing d_sigma_yield (local random barrier) ---///
		///=========================================================///
		if(STOCHASTIC_DYNAMICS==2){
			cout<<" >> Initializing-Local Random Barrier !"<<endl;	
			kernel_Initializing_loca_random_barrier<<<grid,block>>>(d_sigma_yield, time_counter);
			cudaMemcpy(h_sigma_yield_Distrbt, d_sigma_yield, sizeof(double)*N, cudaMemcpyDeviceToHost);
			string name_tst = "tst_yield.dat";
			ofstream file_tst(name_tst.c_str());
			for(int i=0;i<N;i++){
				file_tst<<h_sigma_yield_Distrbt[i]<<endl;			
			}
			cout<<" >> Initializing-Local Random Barrier Success !"<<endl;
		}

		///========================================///
		///--- Main-Loop : Lanch The Experiment ---///
		///========================================///	
		cout<<" >> ShearrateFix_Response_Simulation : Enter The Main loop - Lanching the Experiment ! " <<endl;
		while(t<Tmax){
			if(step%step_delta_supervision==0&&SHOW_STEP!=0){
				cout<<"  >> ================================================================================="<<endl;				
				cout<<"  >> ShearrateFix_Response_Simulation - In Main-Loop : Step/Total = "<<step<<" /"<<ceil(Tmax/dt)<<" ; The exponent of gamma_p = "<<THE_EXPONENT<<endl;
				cout<<"  >> ================================================================================="<<endl;
			}
			//////================================//////
			//////===== Numerical Mesurement =====//////		
			//////================================//////
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"  >> ShearrateFix_Response_Simulation - In Main-Loop : Numerical Mesrurement "<<endl;}
			/////---Instaneous Everage Over all sites (over space)---/////
			//: Stress ://
			if( IEOS_STRESS!=0 && ((USE_IEOS_OW==1&&(t>=Tmax*ow_ieos_start&&t<=Tmax*ow_ieos_end))||USE_IEOS_OW==0) ){
				if(step%step_delta_ieos_stress==0){
					ieos_stress = Space_Aver_Double(d_sigma);
                			file_IEOS_STRESS<<ieos_stress<<" "<<t<<" "<<gamma_p*t<<endl;
				}
			}

			//: State ://
			if(IEOS_STATE!=0 && ((USE_IEOS_OW==1&&(t>=Tmax*ow_ieos_start&&t<=Tmax*ow_ieos_end))||USE_IEOS_OW==0) ){
				if(step%step_delta_ieos_state==0){
					kernel_copy_bool_double<<<grid,block>>>(d_state, d_state_double);
					ieos_state = Space_Aver_State(d_state_double);
					file_IEOS_STATE<<ieos_state<<" "<<t<<" "<<gamma_p*t<<endl; 
				}
			}

			//: VELOCITY OF STRESS & STRESS-DROP RATE ://
			if( step%step_delta_ieos_VAD==0 && (IEOS_VELOCITY_STRESS!=0||IEOS_SDR!=0) && ((USE_IEOS_OW==1&&(t>=Tmax*ow_ieos_start&&t<=Tmax*ow_ieos_end))||USE_IEOS_OW==0) ){
				ieos_stress_onedtbefore = Space_Aver_Double(d_sigma_onedtbefore);
				ieos_stress_now = Space_Aver_Double(d_sigma);
				ieos_stress_velocity = (ieos_stress_now - ieos_stress_onedtbefore)/dt;  
				if(IEOS_VELOCITY_STRESS!=0){
					file_IEOS_VELOCITY_STRESS << ieos_stress_velocity <<" "<<t<<" "<<gamma_p*t<<endl;
				}
				if(IEOS_SDR!=0){
					if(ieos_stress_velocity>0){
						ieos_sdr = 0.;
					}
					else{
						ieos_sdr = abs(ieos_stress_velocity);
					}
					file_IEOS_SDR << ieos_sdr <<" "<<t<<" "<<t*gamma_p<<endl;
				}
			}
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Numerical Mesrurement : Instaneous Everage Over all sites (over space) >>> Success !"<<endl;}
			
			/////---Local Temporal Correlation Function (everaged over space-sites) Of Stress---//////
			if(LTCF_STRESS!=0){
				// - Save the sigma at t_w :
				if(step==step_w){
					cout<<"    >> -----Time_Wait is here : t_w = "<<t_w<<" Step ="<<step<<endl;
					cudaMemcpy(d_sigma_w, d_sigma, sizeof(double)*N, cudaMemcpyDeviceToDevice);
					space_aver_sigma_w = Space_Aver_Double(d_sigma_w);
					kernel_multiplication_double<<<grid,block>>>(d_sigma_w, d_sigma_w, d_sigma_w_2);
					space_aver_sigma_w_2 = Space_Aver_Double(d_sigma_w_2);
					fluctuation_w = space_aver_sigma_w_2 - space_aver_sigma_w*space_aver_sigma_w;				
				}	
				// - Compute the correlation value
				if(((step-step_w)%step_delta_tmp_corre)==0&&step>=step_w){
					kernel_multiplication_double<<<grid,block>>>(d_sigma_w, d_sigma, correlation);
					space_aver_produit = Space_Aver_Double(correlation);
					correlation_space_aver = space_aver_produit-space_aver_sigma_w*Space_Aver_Double(d_sigma);
					correlation_space_aver = correlation_space_aver/fluctuation_w;
					file_LTCF_STRESS<<correlation_space_aver<<" "<<t-t_w<<" "<<gamma_p*(t-t_w)<<" "<<t_w<<endl;			
				}
			}
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Numerical Mesrurement : Correlation Function >>> Success ! "<<endl;}

			/////---Instaneous Physical Quantities Field---/////
			//: Stress ://
			if( IPQF_STRESS!=0 && step%step_delta_ipqf_stress==0 && ((USE_IPQF_OW==1&&(t>=Tmax*ow_ipqf_start&&t<=Tmax*ow_ipqf_end))||USE_IPQF_OW==0) ){	
				cudaMemcpy(h_sigma, d_sigma, sizeof(double)*N, cudaMemcpyDeviceToHost);
				for(int s=0;s<N;s++){
					file_IPQF_STRESS<<h_sigma[s]<<" "; 				
				}
				file_IPQF_STRESS<<t<<endl;
			}
			//: State ://
			if( IPQF_STATE!=0 && step%step_delta_ipqf_state==0  && ((USE_IPQF_OW==1&&(t>=Tmax*ow_ipqf_start&&t<=Tmax*ow_ipqf_end))||USE_IPQF_OW==0) ){
				counter_ipqf_state+=1;	
				cudaMemcpy(h_state, d_state, sizeof(bool)*N, cudaMemcpyDeviceToHost);
				for(int i=0;i<Lx;i++){
					for(int j=0;j<Ly;j++){
						for(int k=0;k<Lz;k++){
							if(h_state[i*Ly*Lz+j*Lz+k]==1){
								file_IPQF_STATE<< i <<" "<< j <<" "<< k <<" "<< t <<" "<< gamma_p*t <<endl;
							}
						}
					}
				}
			}
			//: Accumulated Active State ://
			if( IPQF_ACCUM_ACTIV_STATE!=0 && step!=0 ){
				kernel_accum_active_state<<<grid,block>>>(d_accum_active_state, d_state, step);
				if( step%step_delta_ipqf_accum_active_state==0 && ((USE_IPQF_OW==1&&(t>=Tmax*ow_ipqf_start&&t<=Tmax*ow_ipqf_end))||USE_IPQF_OW==0)){
					cudaMemcpy(h_accum_active_state, d_accum_active_state, sizeof(double)*N, cudaMemcpyDeviceToHost);
					for(int i=0;i<N;i++){
						file_IPQF_ACCUM_ACTIVE_STATE<<h_accum_active_state[i]<<" ";
					}
					file_IPQF_ACCUM_ACTIVE_STATE<<step<<endl;
				}
			}
			if( step%step_delta_supervision==0 && SHOW_DETAIL!=0 ){cout<<"    >> Numerical Mesrurement : Instaneous Physical Quantities Field Success !"<<endl;}
		
			/////---Instaneous Local Random-Barrier : Distribution and Distance-Distribution---/////
			//: Random-Barrier Distribution ://
			if( ILRB_RBD!=0 && step%step_delta_ilrb_rbd==0 && ((USE_ILRB_OW==1&&(t>=Tmax*ow_ilrb_start&&t<=Tmax*ow_ilrb_end))||USE_ILRB_OW==0) ){
				
				kernel_produce_sigma_yield_Distrbt<<<grid,block>>>(d_sigma_yield_Distrbt, d_sigma_yield, d_state);
				cudaMemcpy(h_sigma_yield_Distrbt, d_sigma_yield_Distrbt, sizeof(double)*N, cudaMemcpyDeviceToHost);
				
				//file_ILRB_RBD<<t<<" "<<gamma_p*t<<" ";
				for(int i=0;i<N;i++){
					//if(h_sigma_yield_Distrbt[i]!=0){
						file_ILRB_RBD<<h_sigma_yield_Distrbt[i]<<" ";
					//}
				}				
				file_ILRB_RBD<<endl;			
			}		
			//: Distance-Distribution ://
			if( ILRB_DD!=0 && step%step_delta_ilrb_dd==0 && ((USE_ILRB_OW==1&&(t>=Tmax*ow_ilrb_start&&t<=Tmax*ow_ilrb_end))||USE_ILRB_OW==0) ){			
			
				kernel_produce_distance_Distrbt<<<grid,block>>>(d_distance_Distrbt, d_sigma, d_sigma_yield, d_state);
				cudaMemcpy(h_distance_Distrbt, d_distance_Distrbt, sizeof(double)*N, cudaMemcpyDeviceToHost);

				for(int i=0;i<N;i++){
					file_ILRB_DD<<h_distance_Distrbt[i]<<" ";
				}
				//file_ILRB_DD<<step<<endl;
				file_ILRB_DD<<endl;
			}

			/////---Flow Curve---/////
			if(FLOW_CURVE!=0 && (t>=Tmax*cw_fc_start&&t<=Tmax*cw_fc_end) && step%step_delta_fc==0){
				if(IEOS_STRESS!=0){
					stress_st=(stress_st*double(fc_counter)+ieos_stress)/(double(fc_counter)+1.);
				}
				else{
					ieos_stress = Space_Aver_Double(d_sigma);
					stress_st=(stress_st*double(fc_counter)+ieos_stress)/(double(fc_counter)+1.);
				}
				fc_counter+=1;
				//cout<<"fc_counter = "<<fc_counter<<" average_stress = "<<stress_st<<endl;
			}

			//////=========================//////
			//////===== Physical Part =====//////
			//////=========================//////
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"  >> ShearrateFix_Response_Simulation - In Main-Loop : Physical Part "<<endl;	}		
				///// Before evolving d_state, copy it to d_state_backup for computing variables for the current time step, ex: d_gamma_plastic(t+dt) = n(t)*(d_gamma_plastic(t) + (gamma_p + n(t)*d_sigma(t))dt) , the n(t+dt) evolves from n(t) according to the current d_gamma_plastic(t), and d_gamma_plastic(t+dt) evolves from d_gamma_plastic(t) according to n(t), So before updating one of the two, we should keep the its current value for updating the other one 
			cudaMemcpy(d_state_backup, d_state, sizeof(bool)*N, cudaMemcpyDeviceToDevice);
			cudaMemcpy(d_sigma_onedtbefore, d_sigma, sizeof(double)*N, cudaMemcpyDeviceToDevice); //this is just for mesure the velocity

   			///// Computing d_pl_shearate = d_sigma*d_state;
			kernel_plastic_shear_rate<<<grid,block>>>(d_sigma, d_state, d_pl_shearate);
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Physical Part : Computing d_pl_shearate = d_sigma*d_state Success !"<<endl;}	

			///// Stochastic dynamics;
			if(STOCHASTIC_DYNAMICS==1){kernel_stochatic_dynamics<<<grid,block>>>(d_sigma, d_state, dt, time_counter);}
			if(STOCHASTIC_DYNAMICS==2){kernel_stochastic_dynamics_RandomBarrier_B<<<grid,block>>>(d_gamma_plastic, d_sigma, d_state, d_sigma_yield, time_counter);}
			if(STOCHASTIC_DYNAMICS==3){kernel_stochastic_dynamics_RandomBarrier<<<grid,block>>>(d_gamma_plastic, d_sigma, d_state, time_counter);}
			if(STOCHASTIC_DYNAMICS==4){kernel_stochastic_dynamics_RandomBarrier_C<<<grid,block>>>(d_gamma_plastic, d_sigma, d_state, d_sigma_yield, time_counter);}
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Physical Part : Stochastic dynamics Success !"<<endl;}

			///// Fourrier Transfor : d_pl_shearate --> d_pl_shearate_TF
			cufftExecD2Z(P_r2c, d_pl_shearate, d_pl_shearate_TF);
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Physical Part : Fourrier Transfor : d_pl_shearate --> d_pl_shearate_TF Success ! "<<endl;}

			///// Convolution : d_conv_result_TF = d_G*d_pl_shearate_TF
			//kernel_convolution<<<grid,block_convol>>>(d_G_TF, d_pl_shearate_TF, d_conv_result_TF);
			kernel_convolution_2<<<grid_convol,block>>>(d_G_TF, d_pl_shearate_TF, d_conv_result_TF);
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Physical Part : Convolution_TF Success !"<<endl;}
 
			///// Fourrier Transfor : d_conv_result_TF --> d_conv_result
			cufftExecZ2D(P_c2r, d_conv_result_TF, d_conv_result);
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Physical Part : Convolution_TF --> convolution Success !"<<endl;}

			///// Computing d_gamma_plastic		
			if(STOCHASTIC_DYNAMICS_UPDATING_GAMMA_PL==1){kernel_compute_gamma_plastic<<<grid,block>>>(d_gamma_plastic, d_sigma, d_state_backup, gamma_p);}
			if(STOCHASTIC_DYNAMICS_UPDATING_GAMMA_PL==2){kernel_compute_gamma_plastic_B<<<grid,block>>>(d_gamma_plastic, d_sigma, d_state_backup, gamma_p, d_conv_result);}
			if(step%step_delta_supervision==0&&SHOW_DETAIL!=0){cout<<"    >> Physical Part : Computing d_gmma_plastic Success !"<<endl;}

			///// Update the d_sigma+=(gamma_p+d_conv_result)*dt
			kernel_sigma_evolution<<<grid,block>>>(gamma_p, d_conv_result, dt, d_sigma);				

			///// Update t : t+=dt	and Update time_counter : time_counter+ = 1;
   			t+=dt;
			step+=1;
			time_counter++;			
		}
		cout<<"shear_rate = "<<gamma_p<<" stress = "<<stress_st<<endl;
		cout<<"counter_ipss = "<<counter_ipss<<endl;
		return stress_st;
	}

////++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
///++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


};


