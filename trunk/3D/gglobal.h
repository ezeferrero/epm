///!!!!! : ATTENTION :  if you change the name of this file "gglobal.h" , you should imperatively change the name in function 'SIMULATION_INFO()' in "Amorphous_System_Mesurement_kernels.h";

/////////==========================///////////    
/////////==== GLOBAL VARIABLES ====///////////
/////////==========================///////////
//////////////////////////////////////////////

///!!!!!!!!!!!!!!!!!!!!!!/// 
///!!!! SIMUATION ID !!!!///
///!!!!!!!!!!!!!!!!!!!!!!///
const int SIMULATION_ID_NUMBER = 99903204;

///*****************///
///** SYSTEM SIZE **///	
///*****************///
	 const int Lx = 32;
	 const int Ly = 32;
	 const int Lz = 32; 
	 const int N = Lx*Ly*Lz ;
	 const int N_TF = Lx*Ly*(Lz/2+1) ;
	 const double scale = 1./double(N) ;
	 const double Va = 1.;

///***********************************///
///** NUMERICAL SIMULATION SETTINGS **///
///***********************************///
	///Elementary time step
	 const double dt = 0.01;

	///GPU-Paralization Settings  
	 const int Max_thread = 67107840;
	 const int Max_grid = 65535;
	 const int Max_block = 1024;
	 const int Max_block_z = 64;
	
	 const int TILE_X = 8;
 	 const int TILE_Y = 8;
	 const int TILE_Z = 16;

///*************************///
///** PHYSICAL PARAMETERS **///
///*************************///

	///choose which stochastic dynamics ?  Controled by :
	///	-STOCHASTIC_DYNAMICS = 1 (PICARD'S RULE); 2(ALEXANDRE-Chen'S RULE or Real-ALEXANDRE'S RULE); 3(FALSE-ALEXANDRE RULE); 4(Alexander-Eze Rule for removing the shear banding)
	///	-STOCHASTIC_DYNAMICS_UPDATING_GAMMA_PL = 1(ALEXANDRE-Chen'S RULE); 2(real-ALEXANDRE'S RULE)
	 const int STOCHASTIC_DYNAMICS = 2; 	
	 const int STOCHASTIC_DYNAMICS_UPDATING_GAMMA_PL = 2;

	///shear modulus///
	 const double mu_shear = 1.;

	///for Picard stochastic dynamics
	 const double sigma_y = 1.;
	 const double tau = 1.;
	 const double t_res = 1. ;
	 const double t_pl = 1. ;
	
	///for Alex stochastic dynamics (also for false-Alexandre rule)
	 const double gamma_c = 0.035;//0.07;
	 const double lamda = 702;//701.67473077;
	 const double Emin = mu_shear*gamma_c*gamma_c/4.;
	 const double sigma_min = mu_shear*gamma_c ;

///*****************************************///
///** EXPERIMENTAL(simulation) CONDITIONS **///
///*****************************************///

	///== Constant Driving Options ==///
        ///Repeat the simulation for K times - and for each time use propabily different gamma_p
        const int K = 1;
        ///Shear rate gamma_p starts from 10 power expo_start
        const double expo_start = -2.0;
        ///Shear rate gamma_p ends at 10 power expo_end
        const double expo_end = -4.2;
        ///Total deformation
        const double Gamma_total = 50;

///************************************************///		Each mesurement option corresponds to a boolenne variabl. 
///** SIMULATION CONTROL OF NUMERICAL MESUREMENT **///		When it is 0, we don't do this mesurement. 
///************************************************///		When it is 1, we    do    this mesurement
 	
	/////--Instaneous Average Over Space--/////
		//: IEOS_OW ://
		const bool USE_IEOS_OW = 1;		//For each group of measurement there is the bool control variable USE_XXX_OW, to tell wether we do measurement within an observation window.
		const double ow_ieos_start = 0.3;	//Observation window starts at ow_xxx_start*total steps of simulation.
		const double ow_ieos_end = 1.;		//Observation window ends at ow_xxx_end*total steps of simulation.
		//: STRESS ://
		const bool IEOS_STRESS = 1;
		const double delta_t_ieos_stress = dt;					//For each measurement, we define a temporal resolution of sampling in terms of real time 
		const int step_delta_ieos_stress = ceil(delta_t_ieos_stress/dt);	//Temporal resolution of sampling in terms of simulation step	
		//: STATE ://
		const bool IEOS_STATE = 1;
		const double delta_t_ieos_state = dt;
		const int step_delta_ieos_state = ceil(delta_t_ieos_state/dt);
		//: VELOCITY OF STRESS ://
		const bool IEOS_VELOCITY_STRESS = 1;
		const double delta_t_ieos_VAD = dt;
		const int step_delta_ieos_VAD = ceil(delta_t_ieos_VAD/dt);
		//: STRESS-DROP RATE ://
		const bool IEOS_SDR = 0;

	/////--Local Temporal Correlation Function (everaged over space-sites) Of Stress--//////
		const bool LTCF_STRESS = 0;
		//- Defining the waitting time (and the corresponding iteration step)
		const double Gamma_waiting = 2. ;
		const double t_w = 102.;
		const int step_w = ceil(t_w/dt);
		//- Defining time interval between two successive mesuring of correlation function
		const double delta_t_tmp_corre = dt;
		const int step_delta_tmp_corre = ceil(delta_t_tmp_corre/dt);

	/////--Instaneous Physical Quantities Field--/////
		//: IPQF_OW ://
		const bool USE_IPQF_OW = 1;
		const double ow_ipqf_start = 0.7;
		const double ow_ipqf_end = 1.0;
		//: General Time resolution ://
		const double delta_t_ipqf = dt ;
		const int step_delta_ipqf = ceil(delta_t_ipqf/dt) ;
		//: STRESS ://
		const bool IPQF_STRESS = 0;
		const double delta_t_ipqf_stress = delta_t_ipqf ; 
		const int step_delta_ipqf_stress = step_delta_ipqf ;
		//: STATE ://
		const bool IPQF_STATE = 0;
		const double delta_t_ipqf_state = delta_t_ipqf ; 
		const int step_delta_ipqf_state = step_delta_ipqf ;
		//: Accumulated Active State ://
		const bool IPQF_ACCUM_ACTIV_STATE = 0;
		const int step_delta_ipqf_accum_active_state = 50000;
		//: Stress threshold ://
		const bool IPQF_STRESS_threshold = 0;
		//: Gamma plastic ://
		const bool IPQF_GAMMA_PLASTIC = 0;

	/////--Instaneous Local Random-Barrier : Distribution and Distance-Distribution--/////
		//: ILRB_OW ://
		const bool USE_ILRB_OW = 1;
		const double ow_ilrb_start = 0.7;
		const double ow_ilrb_end = 1.0;
		//: General Time Resolution ://
		const double delta_t_ilrb = 2000*dt;
		const int step_delta_ilrb = ceil(delta_t_ilrb/dt) ;	
		//: Random-Barrier Distribution ://
		const bool ILRB_RBD = 0;
		const double delta_t_ilrb_rbd =  delta_t_ilrb ;
		const int step_delta_ilrb_rbd =  step_delta_ilrb;
		//: Distace-Distribution ://
		const bool ILRB_DD = 0;
		const double delta_t_ilrb_dd = delta_t_ilrb ;
		const int step_delta_ilrb_dd =  step_delta_ilrb;

	/////--Instaneous Population of certain (sigma, state): list of (sigma, state) for all sites on squence of moments--/////
		//: IPSS_OW ://
		const bool USE_IPSS_OW = 0;
		const double ow_ipss_start = 0.3;
		const double ow_ipss_end = 0.6;
		//: Time Resolution ://
		const double delta_t_ipss = 10000*dt;
		const int step_delta_ipss = ceil(delta_t_ipss/dt) ;
		//: IPSS ://
		const bool IPSS = 0;
	
	/////--Flow Curve--/////
		//: Computting Window ://
		const double cw_fc_start = 0.6;
		const double cw_fc_end = 1.;

		const bool FLOW_CURVE = 1;
		const double delta_t_fc = delta_t_ieos_stress ;
		const int step_delta_fc = step_delta_ieos_stress;

///************************************************///
///** SIMULATION-SUPERVISION CONTROL PARATMETERS **///
///************************************************///

	///Supervision Step-Interval
	const int step_delta_supervision = 100000;	
	///Supervision Time-Interval
	const double delta_t_supervision = 100000*dt;
	///Show Step or not
	const bool SHOW_STEP = 1;
	///Show Details of Iteration Step Or Not
	const bool SHOW_DETAIL = 0;


