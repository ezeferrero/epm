using namespace std;
using namespace thrust::placeholders;

#include "gglobal.h"

/////////////////////////////////////////////////////////
//////========== RANDOM NUMBER GENERATOR ==========//////
/////////////////////////////////////////////////////////
__device__ void PhiloxRandomPair(const unsigned int index, const unsigned long time_counter, double *r1, double *r2){

	RNG2 rng;
	RNG2::ctr_type c_pair = {{}} ;
	RNG2::ctr_type r_pair ;
	RNG2::key_type k_pair = {{}} ;
	
	/// keys = threadId 
	k_pair[0] = index ;
	// time counter
	c_pair[0] = time_counter ;
	c_pair[1] = PHILOX_SEED; // or do : PHILOX_SEED + sample ;
	// random number generation
	r_pair = rng(c_pair, k_pair) ;
	
	*r1 = u01_open_open_64_53(r_pair[0]) ;
	*r2 = u01_open_open_64_53(r_pair[1]) ;

//	printf("index = %d, time_counter = %d, r1 = %f, and r2 = %f\n", index, time_counter, *r1,*r2);

}

//////////////////////////////////////////////////////////////////
//////========== KERNEL FUNCTIONS FOR MEASUREMENT ==========//////
//////////////////////////////////////////////////////////////////
double Space_Aver_Double(double *d_Q){
	
	thrust::device_ptr<double> Q_ptr (d_Q);
	double sum = thrust::reduce(Q_ptr, Q_ptr + N);
	return sum/double(N);	
}

double Space_Aver_State(double *d_state_double){

        thrust::device_ptr<double> state_ptr (d_state_double);
        double sum = thrust::reduce(state_ptr, state_ptr + N);
//      cout<<"Moyen: "<<sum/double(N)<<endl;
        return sum/double(N) ;
}

__global__ void kernel_Initializing_loca_random_barrier(double *d_sigma_yield, unsigned long time_counter){

	double r1, r2;
	double sigma_y ;	 

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

	if(i<Lx && j<Ly && k<Lz){

		PhiloxRandomPair(index, time_counter, &r1, &r2);
		sigma_y = sqrt((mu_shear*gamma_c)*(mu_shear*gamma_c)-log(1.-r2)*4.*mu_shear/lamda);
		d_sigma_yield[index] = sigma_y;
	}
	else{
		printf("faut in the kernel stochastic dynamics random barrier, index = %d\n", index);
	}

}

__global__ void kernel_copy_bool_double(bool *d_state, double *d_state_double){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;
	int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

        d_state_double[index] = double(d_state[index]);

}

__global__ void kernel_multiplication_double(double *d_sigma_w, double *d_sigma, double *correlation){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;	

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;
	
	if(i<Lx && j<Ly && k<Lz){

		correlation[index] = d_sigma_w[index] * d_sigma[index] ;
	}
	else{
		printf("faut in kernel plastic shear rate, index = %d\n",index);
	}

}

__global__ void kernel_accum_active_state(double *d_accum_active_state, bool *d_state, int step){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;
	int k = blockIdx.z*blockDim.z+threadIdx.z;

	int index = k*Ly*Lx+j*Lx+i;
	if(i<Lx && j<Ly && k<Lz){

		d_accum_active_state[index] = (d_accum_active_state[index]*double(step-1)+double(d_state[index]))/double(step);
	}
}

__global__ void kernel_produce_sigma_yield_Distrbt(double *d_sigma_yield_Distrbt, double *d_sigma_yield, bool *d_state){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;
	int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

	d_sigma_yield_Distrbt[index] = (1-d_state[index])*d_sigma_yield[index];	
}

__global__ void kernel_produce_distance_Distrbt(double *d_distance_Distrbt, double *d_sigma, double *d_sigma_yield, bool *d_state){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;
	int k = blockIdx.z*blockDim.z+threadIdx.z;
	
//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

//	d_distance_Distrbt[index] = (1-d_state[index])*(d_sigma_yield[index] - d_sigma[index]);
	d_distance_Distrbt[index] = d_sigma_yield[index] - d_sigma[index];
}

////////////////////////////////////////////////////////////////////
//////========== KERNEL FUNCTIONS FOR PHYSICAL PART ==========//////
////////////////////////////////////////////////////////////////////
__global__ void kernel_plastic_shear_rate(cufftDoubleReal *d_sigma, bool *d_state, cufftDoubleReal *d_pl_shearate){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
	int j = blockIdx.y*blockDim.y+threadIdx.y;
	int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;	

	if(i<Lx && j<Ly && k<Lz){

		d_pl_shearate[index] = d_state[index] * d_sigma[index] / tau ;
	}
	else{
		printf("faut in kernel plastic shear rate, index = %d\n",index);
	}

}

__global__ void kernel_stochatic_dynamics(cufftDoubleReal *d_sigma, bool *d_state, double dt, unsigned long time_counter){

	double r1, r2 ; 

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

	if(i<Lx && j<Ly && k<Lz){

		PhiloxRandomPair(index, time_counter, &r1, &r2);
		if(d_state[index]==0){
			if(d_sigma[index]>sigma_y && t_pl*r2<dt){d_state[index]=1;}
		}
		else{
			if(t_res*r2<dt){d_state[index]=0;}
		}
	}
	else{
		printf("faut in kernel stochastic dynamics, index = %d\n",index);
	}

}

__global__ void kernel_stochastic_dynamics_RandomBarrier(double *d_gamma_plastic, cufftDoubleReal *d_sigma, bool *d_state, unsigned long time_counter){

	double r1, r2;
	double sigma_y ;	
	double sigma_y_dimless; 

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

	if(i<Lx && j<Ly && k<Lz){

		if(d_state[index]==0){
			PhiloxRandomPair(index, time_counter, &r1, &r2);
			sigma_y = sqrt(4.*mu_shear*(Emin-log(1.-r2)/lamda));
			sigma_y_dimless = sigma_y/sigma_min;
			if(d_sigma[index]>sigma_y_dimless){d_state[index]=1;}
		}
		else{
			if(d_gamma_plastic[index]>1){d_state[index]=0;}
		}
	}
	else{
		printf("faut in the kernel stochastic dynamics random barrier, index = %d\n", index);
	}

}

__global__ void kernel_stochastic_dynamics_RandomBarrier_B(double *d_gamma_plastic, cufftDoubleReal *d_sigma, bool *d_state, double *d_sigma_yield, unsigned long time_counter){

	double r1, r2;
	double sigma_y ;	 

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

	if(i<Lx && j<Ly && k<Lz){

		if(d_state[index]==0){

			if(abs(d_sigma[index])>d_sigma_yield[index]){d_state[index]=1;}
		}
		else{
			if(abs(d_gamma_plastic[index])>gamma_c){
				PhiloxRandomPair(index, time_counter, &r1, &r2);
				d_state[index]=0;
				sigma_y = sqrt((mu_shear*gamma_c)*(mu_shear*gamma_c)-log(1.-r2)*4.*mu_shear/lamda);
				d_sigma_yield[index] = sigma_y;
			}
		}
	}
	else{
		printf("faut in the kernel stochastic dynamics random barrier, index = %d\n", index);
	}

}

__global__ void kernel_stochastic_dynamics_RandomBarrier_C(double *d_gamma_plastic, cufftDoubleReal *d_sigma, bool *d_state, double *d_sigma_yield, unsigned long time_counter){

	double r1, r2;
	double sigma_y ;	

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

	if(i<Lx && j<Ly && k<Lz){

		PhiloxRandomPair(index, time_counter, &r1, &r2);
//		printf("philox random number r1 = %f, r2 = %f\n", r1, r2);
		if(d_state[index]==0){

			if(abs(d_sigma[index])>d_sigma_yield[index]){d_state[index]=1;}
		}
		else{
			if(abs(d_gamma_plastic[index])>gamma_c || t_res*r1<dt){
				
				d_state[index]=0;
				sigma_y = sqrt((mu_shear*gamma_c)*(mu_shear*gamma_c)-log(1.-r2)*4.*mu_shear/lamda);
				d_sigma_yield[index] = sigma_y;
			}
		}
	}
	else{
		printf("faut in the kernel stochastic dynamics random barrier, index = %d\n", index);
	}

}

__global__ void kernel_convolution_2(cufftDoubleComplex *d_G, cufftDoubleComplex *d_pl_shearate_TF, cufftDoubleComplex *d_conv_result_TF){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

	int index = k*Ly*Lx+j*Lx+i ;
//	int index = i*Ly*(Lz/2+1)+j*(Lz/2+1)+k ;
	
	if(i<Lx && j<Ly && k<Lz/2+1){

		d_conv_result_TF[index].x = scale * (d_G[index].x * d_pl_shearate_TF[index].x - d_G[index].y * d_pl_shearate_TF[index].y) ;
		d_conv_result_TF[index].y = scale * (d_G[index].x * d_pl_shearate_TF[index].y + d_G[index].y * d_pl_shearate_TF[index].x) ;
	}
	else{
//		printf("faut in kernel sigma evolution, index = %d\n",index);
	}
}

__global__ void kernel_compute_gamma_plastic(double *d_gamma_plastic, cufftDoubleReal *d_sigma, bool *d_state, double gamma_p){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;
	
	if(i<Lx && j<Ly && k<Lz){

		//d_gamma_plastic[index]=d_state[index]*2*(d_gamma_plastic[index]+(gamma_p+d_state[index]*d_sigma[index])*dt);
		//d_gamma_plastic[index]=d_state[index]*(d_gamma_plastic[index]+2*(gamma_p+d_state[index]*d_sigma[index])*dt);
		d_gamma_plastic[index]=d_state[index]*(d_gamma_plastic[index]+2*(gamma_p+d_state[index]*d_sigma[index]/(mu_shear*tau))*dt);
	}
	else{
		printf("faut in the kernel compute gamma plastic, index = %d\n", index);
	}

}

__global__ void kernel_compute_gamma_plastic_B(double *d_gamma_plastic, cufftDoubleReal *d_sigma, bool *d_state, double gamma_p, cufftDoubleReal *d_conv_result){
	
	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;

	if(i<Lx && j<Ly && k<Lz){

		d_gamma_plastic[index]=d_state[index]*(d_gamma_plastic[index]+abs(2*(gamma_p/2.+ Va*d_conv_result[index]/(2.*mu_shear)+d_state[index]*d_sigma[index]/(2.*mu_shear*tau)))*dt);
	}
	else{
		printf("faut in the kernel compute gamma plastic, index = %d\n", index);
	}

}

__global__ void kernel_sigma_evolution(double gamma_p, cufftDoubleReal *d_conv_result, double dt, cufftDoubleReal *d_sigma){

	int i = blockIdx.x*blockDim.x+threadIdx.x;
        int j = blockIdx.y*blockDim.y+threadIdx.y;
        int k = blockIdx.z*blockDim.z+threadIdx.z;

//	int index = i*Ly*Lz+j*Lz+k ;
	int index = k*Ly*Lx+j*Lx+i ;	

	if(i<Lx && j<Ly && k<Lz){

		d_sigma[index] = d_sigma[index] + (mu_shear*gamma_p + Va*d_conv_result[index])*dt ;
	}
	else{
		printf("faut in kernel sigma evolution, index = %d\n",index);
	}

}

////////////////////////////////////////////////////////////////////////
//////========== CREATING FILE FOLDOR & INFOMATION FILE ==========//////
////////////////////////////////////////////////////////////////////////
char* folder_name_sim_id_producer(){

	std::stringstream sim_id_num;
        sim_id_num<<SIMULATION_ID_NUMBER;
        std::string name_sim_id = sim_id_num.str();
	std::string name_folder_sim_id = "SIMID_" + name_sim_id;
	char* dirname ;
	dirname = (char*)name_folder_sim_id.c_str();
		
	return dirname;
}

void INFORMATION_FILE(){
	
	char* dirname ;
	dirname = folder_name_sim_id_producer();		
	string str_dirname = dirname;
	string name_info  = str_dirname + "/SIMULATION_INFO.txt";
	ofstream file_info(name_info.c_str());

	string line;
	ifstream infofile ("gglobal.h");
	if(infofile.is_open()){
		while(getline(infofile,line)){
			file_info << line << endl;
		}
		infofile.close();	
	}
}

