/* main_gpu_lab.cu together with its libraries Amorphous_System.cuh & Amorphous_System_kernels_*.cuh,
 * is a CUDA implementation of a 3D elasto-plastic (EP) model.
 * The program simulates a 3-dimensional amorphous solid sheared at a fix strain-rate.
 * A scalar field represents the value of the shear-stress in each block of a square lattice.
 * An overdamped Eulerian dynamics is proposed for the scalar shear-stresses.
 * Long-range interactions are solved using a pseudo-spectral method, that renders the equation
 * of movement to be local in Fourier space. We make intensive use of the cuFFT library.
 * Local yielding stresses are taken randomly from a given distribution after each local yielding,
 * to that end the counter based PRNG "Philox" is used.
 *
 * Started on: Oct 2014 by cliu, eferrero & kmartens.
 * Copyright (C) 2016 Chen Liu, Ezequiel E. Ferrero, Francesco Puosi, Jean-Louis Barrat and Kirsten Martens.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This code was originally implemented for: "Driving rate dependence of avalanche statistics and shapes at the yielding transition",
 * C. Liu, E.E. Ferrero, F. Puosi, J.-L. Barrat and K. Martens
 * http://arxiv.org/abs/1506.08161
 * Phys. Rev. Lett. XX, XXXX (2016)
 * http: xxxxxxxx
 * DOI: xxxxxxxx
 * The implementation is justified in this Supplemental Material
 * http://www-liphy.ujf-grenoble.fr/pagesperso/martens/documents/liu2015-sm.pdf
 * http://prl.aps.org/supplemental/PRL/xxxxxxxx
 * Please cite when appropriate.
 */



// RNG: PHILOX
#include "common/RNGcommon/Random123/philox.h"
#include "common/RNGcommon/Random123/u01.h"

#include<stdio.h>

// For creatting folder, with "mkdir(dirname, 0755);" in main
#include <sys/stat.h>
#include <sys/types.h>

#include <sstream>
#include <iostream> 	/* std::cout, std::fixed */
#include <iomanip>		/* std::setprecision */
#include <cmath>
#include <math.h>
#include <fstream>
#include <string>
#include <assert.h>
#include <stdint.h> 	/* uint8_t */
#include <cufft.h>
#include <cstdlib>
#include <time.h>
#include <ctime>
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/find.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/binary_search.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/logical.h>
#include <thrust/extrema.h>
#include <thrust/functional.h>
#include <thrust/unique.h>

//Define Philox Sdd (double precision)
typedef r123::Philox2x64 RNG2;
typedef r123::Philox4x64 RNG4;
#define PHILOX_SEED 1332277895LLU

#include "common/cuda_util.h"
#include "common/timers.hpp"
#include "Amorphous_System.cuh"

using namespace std;
using namespace thrust::placeholders;

int main(){


	/// Creartting A folder For saving data of simulation of this time :: this folder name is not global, once you change the folder name here, you should imperatively change the name in "Amorphous_System.cuh": the chapiter : "Preparing The Files Saving The Mesurement Results of Simulation and Corresponding Variables" & the name in the function void INFORMATION_FILE()
		char* dirname ;
		dirname = folder_name_sim_id_producer();
		mkdir(dirname, 0755);		
		cout<<">> Creation of directory, with the name of Simulation_ID : "<< dirname <<endl;

	/// Creatting in the folder created above a file for flow curve
		string str_dirname = dirname;
		string name_flowcurve = str_dirname + "/flow_curve.dat";
		ofstream file_flowcurve(name_flowcurve.c_str());
	
	/// Writting all informations in a file SIMULATION_INFO.dat   -------------------------------------------------???????????????????????????????????????????
		INFORMATION_FILE();		
	
	/// Declearation of experimental condition
		double gamma_p;
		double Tmax;
	
	/// Declearation of Physcal Quantities and Defining the intrinsec properties: Propagator
		Amorphous_System as;
		as.Propagator_Initialization();
	
	/// Pre-Initialization of Physical Quantities
		as.InitializationZero();

	/// Do Simulation		

////============================================================================================================================================================////
////============================================================================================================================================================////
			
		cout << ">> Enter the experimental cycles -$CONSTANT_DRIVING$- : " <<endl;
		//: K is total number of repetition of simulation
		for(int nk=0;nk<K;nk++){
		
			double stress_average;
			double THE_EXPONENT = expo_end + (double(K)-double(nk))*((expo_start-expo_end)/double(K));
			gamma_p = pow(10., THE_EXPONENT);
			Tmax = Gamma_total/gamma_p;
	
			cout<<endl;
			cout<< "******************************************************************************"<<endl;
			cout<< "******************************************************************************"<<endl;
			cout<< ">> nk/K = " << nk <<" /"<< K << "; gamma_p =10e " <<THE_EXPONENT<<" = "<<gamma_p<<"; Tmax = "<<Tmax<<" Total_step="<<ceil(Tmax/dt)<<" ; dt = "<<dt<<endl;
			cout<< "******************************************************************************"<<endl; 
			cout<< "******************************************************************************"<<endl;
			
			/// Initialization of Physical Quantities
			//as.InitializationZero();

			/// DoExperiment : Response To A Fixed Shear Rate 
			stress_average = as.ShearrateFix_Response_Simulation(gamma_p, Tmax, THE_EXPONENT);
			file_flowcurve<<gamma_p<<" "<<stress_average<<endl; 
			cout<<" Average_Stress = "<<stress_average<<endl;
	
		}

////===========================================================================================================================================================////
////===========================================================================================================================================================////	

	cout<<endl;
	cout<<"==================================================== "<<endl;	
	cout<<"      THE SIMULATION IS FINISHED !  ^____^  !        "<<endl;
	cout<<"==================================================== "<<endl;
	cout<<endl;

	return 0;
}

