/*
 * mesoplastic_library.cuh
 *
 * Started on: Oct 2014 by cliu, eferrero & kmartens.
 * Copyright (C) 2016 Chen Liu, Ezequiel E. Ferrero, Francesco Puosi, Jean-Louis Barrat and Kirsten Martens.
 */

#ifndef MESOPLASTIC_LIBRARY_CUH
#define MESOPLASTIC_LIBRARY_CUH

// RNG: PHILOX
#include "RNGcommon/Random123/philox.h"
#include "RNGcommon/Random123/u01.h"

#include <iostream> 	/* std::cout, std::fixed */
#include <iomanip>		/* std::setprecision */
#include <cmath>
#include <fstream>
#include <string>

#include <assert.h>
#include <stdint.h> 	/* uint8_t */
#include <cufft.h>
#include <cstdlib>
#include <time.h>

#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/transform.h>
#include <thrust/transform_reduce.h>
#include <thrust/find.h>
#include <thrust/scan.h>
#include <thrust/sort.h>
#include <thrust/binary_search.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/functional.h>
#include <thrust/unique.h>

//Define Philox Seed
typedef r123::Philox2x32 RNG2;
typedef r123::Philox4x32 RNG4;
#ifndef PHILOX_SEED
#define PHILOX_SEED 125522117
#endif

#include "cuda_util.h"
#include "mesoplastic_kernels.cuh"
#include "writeppm.h"
#include "histo.h"

using namespace std;
using namespace thrust::placeholders;

//////////////////////////////////////////////////////////////////////
class mesoplastic_model
{
	private:
	cufftHandle plan_c2r;
	cufftHandle plan_r2c;

	public:
	// Pointer to host arrays (realspace)
	bool *h_state;
	REAL *h_sigma;
	REAL *h_propagator;

	// Pointer to device arrays (realspace and fourierspace)
	bool *d_state;
	bool *d_state_aux;
	unsigned int *d_clock;
	REAL *d_sigma;
	REAL *d_sigma_threshold;
	REAL *d_accstrain;
	COMPLEX *d_dsigma;
	REAL *d_dsigma_r;
	COMPLEX *d_epsilon_dot;
	REAL *d_epsilon_dot_r;
	REAL *d_propagator;

	// intializing the class
	mesoplastic_model(){

		//Array sizes
		size_t size_b  = NN * sizeof(bool);
		size_t size_ui = NN * sizeof(unsigned int);
		size_t size_r  = NN * sizeof(REAL);
		size_t size_c  = LX*(LY/2+1) * sizeof(COMPLEX);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		// Allocate arrays on host
		h_state = (bool *)malloc(size_b);
		h_sigma = (REAL *)malloc(size_r);
		h_propagator = (REAL *)malloc(size_r);

		// Allocate arrays on device
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_state, size_b));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_state_aux, size_b));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_clock, size_ui));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_sigma, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_accstrain, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_sigma_threshold, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_dsigma_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot, size_c));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_epsilon_dot_r, size_r));
		CUDA_SAFE_CALL(cudaMalloc((void **) &d_propagator, size_rc));

		//Preventive fill everything with 0s
		for (unsigned int k=0; k<NN; k++){h_state[k]=0; h_sigma[k]=0.0; h_propagator[k]=0.0;}
		CUDA_SAFE_CALL(cudaMemset(d_state, 0, size_b));
		CUDA_SAFE_CALL(cudaMemset(d_clock, 0, size_ui));
		CUDA_SAFE_CALL(cudaMemset(d_sigma, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_accstrain, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_sigma_threshold, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_dsigma, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_dsigma_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_epsilon_dot, 0, size_c));
		CUDA_SAFE_CALL(cudaMemset(d_epsilon_dot_r, 0, size_r));
		CUDA_SAFE_CALL(cudaMemset(d_propagator, 0, size_rc));

		// cuFFT plan
		#ifdef DOUBLE_PRECISION
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_Z2D);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_D2Z);
		#else
		cufftPlan2d(&plan_c2r, LX, LY, CUFFT_C2R);
		cufftPlan2d(&plan_r2c, LX, LY, CUFFT_R2C);
		#endif
		//cufftSetCompatibilityMode(plan_c2r, CUFFT_COMPATIBILITY_NATIVE);
		//cufftSetCompatibilityMode(plan_r2c, CUFFT_COMPATIBILITY_NATIVE);
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-----------------------Initialize Functions----------------///////////////////////////

	//Initializes on Host and CpyToDevice
	void Initialize(){

		// Initialize trivial host arrays
		for(int i = 0; i<LX; i++)
			for(int j = 0; j<LY; j++)
			{
				int ij = i*LY+j;
				h_sigma[ij] = STRESS_THRESHOLD;
				h_state[ij] = 0;
			}
		h_state[0]=1;
		h_state[LX/2*LY+LY/2]=1;

		// Initialize host array for propagator
		for(int i = 0; i<LX/2+1; i++)
			for(int j = 0; j<(LY/2+1); j++)
			{

			REAL qx=2.0*M_PI*double(i)/LX;
			REAL qy=2.0*M_PI*double(j)/LY;
			REAL q2=qx*qx+qy*qy;

			if(i!=0 || j!=0) h_propagator[i*(LY/2+1)+j] = -4*qx*qx*qy*qy/(q2*q2);
			if(i!=0) h_propagator[(LX-i)*(LY/2+1)+j] = h_propagator[i*(LY/2+1)+j];
			}
		h_propagator[0] =-1.0; // amplitude of the plastic event

		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		size_t size_rc = LX*(LY/2+1) * sizeof(REAL);

		cudaMemcpy(d_sigma, h_sigma, size_r ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_state, h_state, size_b ,cudaMemcpyHostToDevice);
		cudaMemcpy(d_propagator, h_propagator, size_rc, cudaMemcpyHostToDevice);
	}

	unsigned int InitializeOnDevice(){

		// Initialize State
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::fill(state_ptr, state_ptr+NN, 0);
		//Or construct a particular initial state:
		/*thrust::device_vector<bool> tmp(NN, 0);
		tmp[0]=1;
		tmp[LX/2*LY+LY/2]=1; //(random() & 0x1u);
		thrust::copy(tmp.begin(),tmp.end(),state_ptr);
		*/

		// Initialize Stress
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::fill(sigma_ptr, sigma_ptr+NN, 0.0);
		//thrust::fill(sigma_ptr, sigma_ptr+NN, STRESS_THRESHOLD);
		/*dim3 dimBlock0(TILE_X, TILE_Y);
		dim3 dimGrid0(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock0.x*dimBlock0.y<=THREADS_PER_BLOCK);
		assert(dimGrid0.x<=BLOCKS_PER_GRID && dimGrid0.y<=BLOCKS_PER_GRID);
		unsigned int ran = 12345; //time(NULL);
		Kernel_InitRandomGaussian<<<dimGrid0, dimBlock0>>>(d_sigma, d_state, ran);
		*/

		//Initialize AccumulatedStrain
		thrust::device_ptr<REAL> accstrain_ptr (d_accstrain);
		thrust::fill(accstrain_ptr, accstrain_ptr+NN, 0.0);

		#ifdef TIMEFIX
		thrust::device_ptr<unsigned int> clock_ptr (d_clock);
		thrust::fill(clock_ptr, clock_ptr+NN, 0);
		#endif
		//wondering if we should set this to zero after each gammadot change

		// Initialize Random Local Stress Yield
		dim3 dimBlock1(TILE_X, TILE_Y);
		dim3 dimGrid1(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock1.x*dimBlock1.y<=THREADS_PER_BLOCK);
		assert(dimGrid1.x<=BLOCKS_PER_GRID && dimGrid1.y<=BLOCKS_PER_GRID);
		unsigned int ran2 = time(NULL);
		//Kernel_InitRandomExponential<<<dimGrid1, dimBlock1>>>(d_sigma_threshold, ran2);
		Kernel_InitRandomBouchaud<<<dimGrid1, dimBlock1>>>(d_sigma_threshold, ran2);

		thrust::device_ptr<REAL> sigma_threshold_ptr (d_sigma_threshold);
		//thrust::copy(sigma_threshold_ptr, sigma_threshold_ptr+NN, sigma_ptr);

		// Initialize Propagator
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_SetPropagator<<<dimGrid, dimBlock>>>(d_propagator,LX,LY/2+1);
		//TODO: Try to fit propagator in constant or texture memory
		//CUDA_SAFE_CALL(cudaBindTexture(NULL, propagatorTex, d_propagator, LX*(LY/2+1)*sizeof(REAL)));

		return ran2;
	}


////////////////////////////////////////////////////////////////////////////////////////////////

//////////-------------------------Update Functions------------------///////////////////////////


	void UpdateStateVariables(unsigned long long int time){

		CUDA_SAFE_CALL(cudaMemset(d_state_aux, 0, NN * sizeof(bool)));

		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/2/TILE_X, LY/TILE_Y); //using PhiloxPair
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		#ifdef RANDOM_THRESHOLD
		#ifdef TIMEFIX
		Kernel_UpdateStateVariablesRandomThreshold<<<dimGrid, dimBlock>>>(d_sigma, d_sigma_threshold, d_accstrain, d_clock, d_state, time);
		#else
		Kernel_UpdateStateVariablesRandomThreshold<<<dimGrid, dimBlock>>>(d_sigma, d_sigma_threshold, d_accstrain, d_state, d_state_aux, time);
		#endif
		#else
		Kernel_UpdateStateVariables<<<dimGrid, dimBlock>>>(d_sigma, d_state, time);
		#endif
	}

	void ComputePlasticStrain(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<bool> state_ptr (d_state);
		thrust::device_ptr<REAL> epsilon_dot_ptr (d_epsilon_dot_r);
		//TODO: This can be a transform by key or something takig advantage of the bool
		// epsilon_dot <- sigma * state
		thrust::transform(sigma_ptr, sigma_ptr+NN, state_ptr, epsilon_dot_ptr, thrust::multiplies<REAL>());

		//NOTICE:There is a factor of 1/2 missing in the calculation of epsilon_dot, that would cancell with the factor \
		of 2 that is missing in the calculus of the d_dsigma later on. Everyone happy.
	}

	void Convolution(){
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y/2+1);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_ComplexPointwiseMulAndScale<<<dimGrid, dimBlock>>>(d_epsilon_dot, d_propagator, d_dsigma, SCALE);

		//TODO: Do this with Thrust.
	/*	thrust::device_ptr<COMPLEX> epsilon_ptr (d_epsilon_dot);
		thrust::device_ptr<REAL> propagator_ptr (d_propagator);
		thrust::device_ptr<COMPLEX> state_ptr (d_dsigma);
		//AND NOW? SAXPY with complex type???
		thrust::transform(epsilon_ptr, epsilon_ptr+NN, propagator_ptr, state_ptr, 
				SCALE * _1 * _2); // placeholder expression
	*/
	}

	void EulerIntegrationStep(REAL shear_rate){
		//TODO: If the single other term  in the equation of motion besides the convolution is the shear rate, we \
		can perform Euler directly in fourier, applying gamma to the 0 mode, and then antitransform the new sigma.
		dim3 dimBlock(TILE_X, TILE_Y);
		dim3 dimGrid(LX/TILE_X, LY/TILE_Y);
		assert(dimBlock.x*dimBlock.y<=THREADS_PER_BLOCK);
		assert(dimGrid.x<=BLOCKS_PER_GRID && dimGrid.y<=BLOCKS_PER_GRID);
		Kernel_EulerIntegrationStep<<<dimGrid, dimBlock>>>(d_sigma, d_dsigma_r, d_state, d_accstrain, shear_rate, TIME_STEP);
	}

	void TransformToFourierSpace(){
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecD2Z(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
		#else
		CUFFT_SAFE_CALL(cufftExecR2C(plan_r2c, d_epsilon_dot_r, d_epsilon_dot));
		#endif
	}

	void AntitransformFromFourierSpace(){
		#ifdef DOUBLE_PRECISION
		CUFFT_SAFE_CALL(cufftExecZ2D(plan_c2r, d_dsigma, d_dsigma_r));
		#else
		CUFFT_SAFE_CALL(cufftExecC2R(plan_c2r, d_dsigma, d_dsigma_r));
		#endif
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////-------------------------Calculate Functions---------------///////////////////////////

	REAL ComputeSsstress(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		return thrust::reduce(sigma_ptr,sigma_ptr+NN)/(REAL)NN;
	}

	REAL ComputeStressUnnormalized(){
		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::plus<REAL> binary_op;
		REAL init = 0.0;
		return thrust::reduce(sigma_ptr, sigma_ptr+NN, init, binary_op);
	}

	REAL ComputeActivity(){
		thrust::device_ptr<bool> state_ptr (d_state);
		return thrust::reduce(state_ptr,state_ptr+NN, (int) 0)/(REAL)NN;
	}

	int ComputeActivityUnnormalized(){
		thrust::device_ptr<bool> state_ptr (d_state);
		return thrust::reduce(state_ptr,state_ptr+NN, (int) 0);
	}

	int ComputeActivationUnnormalized(){
		thrust::device_ptr<bool> state_ptr (d_state_aux);
		return thrust::reduce(state_ptr,state_ptr+NN, (int) 0);
	}

	void ComputePrintX(ofstream &fstr){

		CUDA_SAFE_CALL(cudaMemcpy(h_state, d_state, NN*sizeof(bool), cudaMemcpyDeviceToHost));

		thrust::device_ptr<REAL> sigma_ptr (d_sigma);
		thrust::device_ptr<REAL> sigma_thresold_ptr (d_sigma_threshold);
		thrust::device_vector<REAL> x(NN);

		thrust::minus<REAL> op;
		thrust::transform(sigma_thresold_ptr, sigma_thresold_ptr + NN, sigma_ptr, x.begin(), op);

		thrust::host_vector<REAL> h_x = x;
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				//fstr << h_sigma[k] << " ";
				fstr << std::setprecision(15) << h_x[k] << " " << h_state[k] << "\n";
			}
			//fstr << endl;
		}
		fstr << endl;
	}

////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Copy Functions------------------///////////////////////////

	/* transfer from GPU to CPU memory */
	void CpyDeviceToHost(){
		size_t size_b = NN * sizeof(bool);
		size_t size_r = NN * sizeof(REAL);
		CUDA_SAFE_CALL(cudaMemcpy(h_state, d_state, size_b, cudaMemcpyDeviceToHost));
		CUDA_SAFE_CALL(cudaMemcpy(h_sigma, d_sigma, size_r, cudaMemcpyDeviceToHost));
	}


////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Print Functions-----------------///////////////////////////

	void PrintTraps(ofstream &fstr){
		CUDA_SAFE_CALL(cudaMemcpy(h_sigma, d_sigma_threshold, NN * sizeof(REAL), cudaMemcpyDeviceToHost));
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				//fstr << h_sigma[k] << " ";
				fstr << h_sigma[k] << endl;
			}
			//fstr << endl;
		}
		fstr << endl;
	};

	void PrintState(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << h_state[k] << '\n';
			}
		}
		fstr << endl;
	};

	void PrintSigma(ofstream &fstr){
		for(int i=0;i<LX;i++){
			for(int j=0;j<LY;j++){
				int k=i*LY+j;
				fstr << std::setprecision(15) << h_sigma[k] << '\n';
			}
		}
		fstr << endl;
	};

	void PrintResults(){
		string outputFile = "sigma.dat";
		ofstream out(outputFile.c_str());
		if(out)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out << h_sigma[j*LX+i] << "\t";
			  out << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;

		string outputFile2 = "state.dat";
		ofstream out2(outputFile2.c_str());
		if(out2)
		{
			for (int j=0; j<LX; j++) 
			{
			  for (int i=0; i<LX; i++) 
			    out2 << h_state[j*LX+i] << "\t";
			  out2 << endl;
			}
		}
		else
		cerr<<"ERROR!"<<endl;
	}

	#ifdef VISUALIZATION
	void PrintRaw(unsigned long long t, REAL time){
		size_t size_r = 32*sizeof(REAL);
		size_t size_b = 32 * sizeof(bool);
		cudaMemcpy(h_sigma, d_sigma, size_r ,cudaMemcpyDeviceToHost);
		cudaMemcpy(h_state, d_state, size_b ,cudaMemcpyDeviceToHost);
		cout << (time*TIME_STEP) << " " << t*TIME_STEP << " " \
		<< h_sigma[0] << " "  << h_state[0] << " "  << h_sigma[1] << " "  << h_state[1] << " "  
		<< h_sigma[2] << " "  << h_state[2] << " "  << h_sigma[3] << " "  << h_state[3] << " "
		<< h_sigma[4] << " "  << h_state[4] << " "  << h_sigma[5] << " "  << h_state[5] << " "
		<< h_sigma[6] << " "  << h_state[6] << " "  << h_sigma[7] << " "  << h_state[7] << " "
		<< h_sigma[8] << " "  << h_state[8] << " "  << h_sigma[9] << " "  << h_state[9] << " "
		<< h_sigma[10] << " "  << h_state[10] << " "  << h_sigma[11] << " "  << h_state[11] << " "
		<< h_sigma[12] << " "  << h_state[12] << " "  << h_sigma[13] << " "  << h_state[13] << " "
		<< endl;
	}

	void Visualization(REAL gamma_p){
		CpyDeviceToHost();

		char filename[128];
		sprintf(filename, "framesigma%5f.ppm", gamma_p);
		PrintPicture(filename,0);
		sprintf(filename, "framestate%5f.ppm", gamma_p);
		PrintPicture(filename,1);
		#ifdef JPEG
		char cmd[256];
		sprintf(cmd, "convert framesigma%5f.ppm framestate%5f.ppm +append frame_Sigma-State%5f.ppm", \
				gamma_p, gamma_p, gamma_p);
		int i = system(cmd);
		//sprintf(cmd, "rm framesigma%5f.ppm framestate%5f.ppm", gamma_p, gamma_p);
		//i = system(cmd);
		#endif
	}

	void PrintPicture(char *picturename, int who){
		if (who==0) writePPMbinaryImage(picturename, h_sigma);
		if (who==1) writePPMbinaryImage_bool(picturename, h_state);
	}

	#endif

////////////////////////////////////////////////////////////////////////////////////////////////

//////////---------------------------Cleaning Issues-----------------///////////////////////////


	~mesoplastic_model(){

		/* frees CPU memory */
		free(h_state);
		free(h_sigma);
		free(h_propagator);

		/* frees GPU memory */
		cudaFree(d_state);
		cudaFree(d_state_aux);
		cudaFree(d_clock);
		cudaFree(d_sigma);
		cudaFree(d_sigma_threshold);
		cudaFree(d_accstrain);
		cudaFree(d_dsigma);
		cudaFree(d_dsigma_r);
		cudaFree(d_epsilon_dot);
		cudaFree(d_epsilon_dot_r);
		cudaFree(d_propagator);

		cufftDestroy(plan_r2c);
		cufftDestroy(plan_c2r);

	};
};


#endif /*  MESOPLASTIC_LIBRARY_CUH */
