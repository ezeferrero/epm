/*
 * main_mesoplastic.cu
 *
/

/* main_mesoplastic.cu together with its libraries mesoplastic_library.cuh & mesoplastic_kernels.cuh,
 * is a CUDA implementation of a 2D elasto-plastic (EP) model.
 * The program simulates a 2-dimensional amorphous solid sheared at a fix strain-rate.
 * A scalar field represents the value of the shear-stress in each block of a square lattice.
 * An overdamped Eulerian dynamics is proposed for the scalar shear-stresses.
 * Long-range interactions are solved using a pseudo-spectral method, that renders the equation
 * of movement to be local in Fourier space. We make intensive use of the cuFFT library.
 * Local yielding stresses are taken randomly from a given distribution after each local yielding,
 * to that end the counter based PRNG "Philox" is used.
 *
 * Started on: Oct 2014 by cliu, eferrero & kmartens.
 * Copyright (C) 2016 Chen Liu, Ezequiel E. Ferrero, Francesco Puosi, Jean-Louis Barrat and Kirsten Martens.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * This code was originally implemented for: "Driving rate dependence of avalanche statistics and shapes at the yielding transition",
 * C. Liu, E.E. Ferrero, F. Puosi, J.-L. Barrat and K. Martens
 * http://arxiv.org/abs/1506.08161
 * Phys. Rev. Lett. XX, XXXX (2016)
 * http: xxxxxxxx
 * DOI: xxxxxxxx
 * The implementation is justified in this Supplemental Material
 * http://www-liphy.ujf-grenoble.fr/pagesperso/martens/documents/liu2015-sm.pdf
 * http://prl.aps.org/supplemental/PRL/xxxxxxxx
 * Please cite when appropriate.
 */


//Defined parameters at compilation time (can be modified in Makefile)
#ifndef LX
#define LX 256
#endif
#ifndef LY
#define LY LX 
#endif

#define NN (LX*LY)
#define SCALE (1.0/NN)

#ifndef TIME_STEP
#define TIME_STEP 0.01
#endif

#ifndef STRESS_THRESHOLD
#define STRESS_THRESHOLD 1.0
#endif

#ifndef STRAIN_THRESHOLD
#define STRAIN_THRESHOLD 0.035 //0.07
#endif

#ifndef E_MIN
#define E_MIN (STRAIN_THRESHOLD*STRAIN_THRESHOLD*0.25)
#endif

#ifndef LAMBDA
#define LAMBDA 701.67473077 //543.56593627 //452.15239728 //2015.16735678 //701.67473077 //2015.16735678 //702
#endif

#ifndef TAU_ELAST
#define TAU_ELAST 1.0
#endif

#ifndef TAU_PLAST
#define TAU_PLAST 1.0
#endif

#ifndef CLOCK_THRESHOLD
#define CLOCK_THRESHOLD ((10/TIME_STEP))
#endif

#ifndef GAMMADOT_MIN
#define GAMMADOT_MIN 0.0001
#endif

#ifndef GAMMADOT_MAX
#define GAMMADOT_MAX 10.00
#endif

#ifndef GAMMADOT_POINTS
#define GAMMADOT_POINTS 80
#endif

#ifndef TRANSIENT_T
#define TRANSIENT_T 2 //10
#endif

#ifndef RUNNING_T
#define RUNNING_T 10 //20
#endif

#ifndef DATA_T
#define DATA_T 0.01 //0.05
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

//TODO: Tunne this to optimize
#define TILE_X 64
#define TILE_Y 8

#ifndef CUDA_DEVICE
#define CUDA_DEVICE 0
#endif

// Functions
#define MAX(a,b) (((a)<(b))?(b):(a))	// maximum
#define MIN(a,b) (((a)<(b))?(a):(b))	// minimum
#define ABS(a) (((a)< 0)?(-a):(a))	// absolute value

// Hardware parameters for Tesla c2075 (GF100) cc 2.0
#define SHARED_PER_BLOCK 49152
#define WARP_SIZE 32
#define THREADS_PER_BLOCK 1024
#define BLOCKS_PER_GRID 65535

#include "mesoplastic_library.cuh"
#include "timers.hpp"

void Equilibrate(mesoplastic_model &T, const REAL gamma_p, const unsigned int t_transient, const unsigned int t_data){

	#ifdef PRINT_TRAPS
	char trapsfilename[128];
	#endif

	for(unsigned int i=0; i<t_transient; i++){
		T.UpdateStateVariables(200000000+i);
		T.ComputePlasticStrain();
		T.TransformToFourierSpace();
		T.Convolution();
		T.AntitransformFromFourierSpace();
		T.EulerIntegrationStep(gamma_p);

		#ifdef PRINT_TRAPS
		if(i%t_data==0){
		sprintf(trapsfilename, "traps_i%d_gdot%8.8f_LX%dLY%d_dt%4.4f.dat", 200000000+i, gamma_p, LX , LY, TIME_STEP);
		ofstream outtraps(trapsfilename);
		T.PrintTraps(outtraps);
		}
		#endif

	}

}

void TimeLoop(mesoplastic_model &T, const REAL gamma_p, const unsigned int t_transient, const unsigned int t_run, const unsigned int t_data, double &ssstress, double &ssstress2){

	char filename[128];

	sprintf(filename, "stress-strain_gdot%8.8f_LX%dLY%d_dt%4.4f.dat", gamma_p, LX , LY, TIME_STEP);
	ofstream outss(filename);
	outss << "## << t*TIME_STEP*gamma_p << " " << sigmaaverage << " " << activity << " " << gamma_p " << endl;

	sprintf(filename, "veloc_gdot%8.8f_LX%dLY%d_dt%4.4f.dat", gamma_p, LX , LY, TIME_STEP);
	ofstream outveloc(filename);
	outveloc << "## << veloc = (sigmaaverage(t)-sigmaaverage(t-dt))/dt << n: total activity << n+:going on activations << s:stress(unnormalized)" << endl;
	outveloc << std::fixed;
	outveloc << std::setprecision(24);

	for(unsigned int i=0; i<t_transient+1; i++){
		T.UpdateStateVariables(i);
		T.ComputePlasticStrain();
		T.TransformToFourierSpace();
		T.Convolution();
		T.AntitransformFromFourierSpace();
		T.EulerIntegrationStep(gamma_p);
	}

	REAL sigmaaux = 0.0;
	unsigned long long int count=0;
	unsigned long long int t=0;
	while(t<t_run+1){

		REAL sigma = T.ComputeStressUnnormalized();
		REAL veloc = (sigma-sigmaaux)/TIME_STEP;
		sigmaaux=sigma;
		int activity = T.ComputeActivityUnnormalized();
		int activations = T.ComputeActivationUnnormalized();

		if(t!=0) outveloc << veloc << " " << activity << " " << activations << " " << sigma <<'\n';

	//Measurements
		if(t%t_data==0){
			REAL sigmaaverage = sigma/(REAL)NN;
			ssstress+= sigmaaverage;
			ssstress2+=sigmaaverage*sigmaaverage;
			count++;
			REAL activityaverage = activity/(REAL)NN;
			outss << t*TIME_STEP*gamma_p << " " << sigmaaverage << " " << activityaverage << " " << gamma_p << endl;
		}

	//Dynamics
		// Update n(x,z;t) state variables: 0/1 corresponds to elastic/plastic
		T.UpdateStateVariables(t);

		#ifdef PRINT_X
		if(t%t_data==0){
			sprintf(filename, "x_i%llu_gdot%8.8f_LX%dLY%d_dt%4.4f.dat", 10000000000000000000+t, gamma_p, LX , LY, TIME_STEP);
			ofstream outx(filename);
			T.ComputePrintX(outx);
		}
		#endif

		t++;
		// Calculation of the plastic strain "epsilon_dot" ~ n*sigma
		T.ComputePlasticStrain();
		// Calculation of the convolution with the propagator and the plastic strain
		// we transform the plastic strain epsilon_dot
		T.TransformToFourierSpace();
		//convolution
		T.Convolution();
		T.AntitransformFromFourierSpace();
		T.EulerIntegrationStep(gamma_p);

	}

	ssstress = ssstress/(REAL)count;
	ssstress2 = ssstress2/(REAL)count;

}


// main routine
int main(){

	//TODO: complete asserts
	assert(TILE_X%2==0);
	assert(TILE_Y%2==0);
	assert(LX%TILE_X==0);
	assert(LY%TILE_Y==0);
	assert(LX/2>=TILE_X);
	assert(LY/2>=TILE_Y);
	//assert(MAX_STEPS%DATA==0);
	//We've assumed a small temporal step in the integration scheme
	assert(TIME_STEP<=0.1);

	// Set the GPGPU computing device
	#ifdef CUDA_DEVICE
	if (CUDA_DEVICE!=55) CUDA_SAFE_CALL(cudaSetDevice(CUDA_DEVICE));
	#endif
	// Choosing less "shared memory", more cache.
	//CUDA_SAFE_CALL(cudaThreadSetCacheConfig(cudaFuncCachePreferL1)); 

	std::setprecision (15);

	//print simulation parameters
	cout << "### LX ................................: " << LX << '\n';
	cout << "### LY ................................: " << LY << '\n';
	cout << "### Time Step..........................: " << TIME_STEP << '\n';
	cout << "### Acc Strain Thershold (gamma_c).....: " << STRAIN_THRESHOLD << '\n';
	cout << "### Lambda from trap distribution......: " << LAMBDA << '\n';
	cout << "### Shear rate min ....................: " << GAMMADOT_MIN << '\n';
	cout << "### Shear rate max ....................: " << GAMMADOT_MAX << '\n';
	cout << "### Shear rate points .................: " << GAMMADOT_POINTS << '\n';
	cout << "### Transient strain...................: " << TRANSIENT_T << '\n';
	cout << "### Running strain.....................: " << RUNNING_T << '\n';
	cout << "### Data aquiring......................: " << DATA_T << '\n';
	#ifdef DOUBLE_PRECISION
	cout << "### DOUBLE PRECISION...................: " << '\n';
	#else
	cout << "### SINGLE PRECISION...................: " << '\n';
	#endif
	cout << "### SEED Philox .......................: " << PHILOX_SEED       << endl;

	// Our class
	mesoplastic_model T;

	// Turn on cronometer
	cpu_timer clock;
	clock.tic();

	char flowcurvefilename [256];
	sprintf(flowcurvefilename, "flowcurve_LX%dLY%d_dt%4.4f.dat",LX , LY, TIME_STEP);
	ofstream outflow(flowcurvefilename);

	REAL exponent_min= log10(GAMMADOT_MIN);
	REAL exponent_step= (log10(GAMMADOT_MAX)-log10(GAMMADOT_MIN))/(REAL)GAMMADOT_POINTS;

	for(int k=GAMMADOT_POINTS; k>=0; k--)
//	for(int k=0; k<=GAMMADOT_POINTS; k++)
	{
		REAL gamma_p=pow(10.0,exponent_min+k*exponent_step);  // shear rate 

		unsigned int t_transient=(int)((TRANSIENT_T/gamma_p)/TIME_STEP+0.5);
		unsigned int t_run=(int)((RUNNING_T/gamma_p)/TIME_STEP+0.5);
		unsigned int t_data= (int)((DATA_T/gamma_p)/TIME_STEP+0.5);

		// Initialization
		unsigned int intranseed = T.InitializeOnDevice();
		cout << "### SEED random() for initial traps....: " << intranseed  << " for shear-rate " << gamma_p  << endl;

		// Equilibration (additional to transient shear)
		Equilibrate(T, gamma_p, 50000, 10000);
//		if (k==GAMMADOT_POINTS) Equilibrate(T, gamma_p, 2000*BIG_TRANSIENT_T, 100*BIG_TRANSIENT_T);
//		if (k==0) Equilibrate(T, gamma_p, TRANSIENT_T, 0.1*TRANSIENT_T);

		double ssstress=0.0;
		double ssstress2=0.0;

		TimeLoop(T, gamma_p, t_transient, t_run,t_data, ssstress, ssstress2);
		outflow << std::setprecision(15) << gamma_p << " "<< ssstress << " " << sqrt(ssstress2-ssstress*ssstress) << " " << ssstress2 << endl;
		// TODO: Check if an 1/sqrt(N) is missing in those error bars

		//Print last states
		T.CpyDeviceToHost();
		char filename [256];
		sprintf(filename, "Stress_i%lu_gdot%8.8f_LX%dLY%d_dt%4.4f.dat", 100000000000000000+t_run, gamma_p, LX , LY, TIME_STEP);
		ofstream outsigma(filename);
		T.PrintSigma(outsigma);
		sprintf(filename, "State_i%lu_gdot%8.8f_LX%dLY%d_dt%4.4f.dat", 100000000000000000+t_run, gamma_p, LX , LY, TIME_STEP);
		ofstream outstate(filename);
		T.PrintState(outstate);

		// Print frames for visualization (includes cudaMemCpyDtoH)
		#ifdef VISUALIZATION
			#warning VISUALIZATION ON HERE
			T.Visualization(gamma_p);
		#endif
	}

	cout << "### ------ CUDA MESO_PLASTIC MODEL------ " << endl;
	// Turn off cronometer
	clock.tac();
	//clock.print();
	REAL total_time = clock.cpu_elapsed();
	cout << "### Total time    : " << total_time  << " ms" << endl;

	return 0;
}
