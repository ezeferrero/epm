#ifdef DOUBLE_PRECISION
typedef double REAL;
typedef double2 REAL2;
typedef cufftDoubleComplex COMPLEX;
#else
typedef float REAL;
typedef float2 REAL2;
typedef cufftComplex COMPLEX;
#endif

/*--------RNG ROUTINES--------*/
__device__
REAL DeviceBoxMuller(const REAL u1, const REAL u2)
{
	REAL r = sqrt( -2.0*log(u1) );
	REAL theta = 2.0*M_PI*u2;
	return r*sin(theta);
}

__device__
void PhiloxRandomQuartet(const unsigned int index, const unsigned long long int time, float *r1, float *r2, float *r3, float *r4)
{
	RNG4 rng;
	RNG4::ctr_type c_pair={{}};
	RNG4::key_type k_pair={{}};
	RNG4::ctr_type r_quartet;

		// keys = threadid
		k_pair[0]= PHILOX_SEED; // eventually do: PHILOX_SEED + sample;
		// time counter
		c_pair[0]= time;
		c_pair[1]= index;
		// random number generation
		r_quartet = rng(c_pair, k_pair);

		*r1= u01_open_open_32_53(r_quartet[0]);
		*r2= u01_open_open_32_53(r_quartet[1]);
		*r3= u01_open_open_32_53(r_quartet[2]);
		*r4= u01_open_open_32_53(r_quartet[3]);
}

__device__
void PhiloxRandomPair(const unsigned int index, const unsigned long long int time, float *r1, float *r2)
{
	RNG2 rng;
	RNG2::ctr_type c_pair={{}};
	RNG2::key_type k_pair={{}};
	RNG2::ctr_type r_pair;

		// keys = threadid
		k_pair[0]= PHILOX_SEED; // eventually do: PHILOX_SEED + sample;
		// time counter
		c_pair[0]= time;
		c_pair[1]= index;
		// random number generation
		r_pair = rng(c_pair, k_pair);

		*r1= u01_open_open_32_53(r_pair[0]);
		*r2= u01_open_open_32_53(r_pair[1]);
}


//---------- DEVICE FUNCTIONS -----------//

//////////-----------------------Initialize Kernels----------------///////////////////////////

__global__ void Kernel_InitRandomGaussian(REAL* d_sigma, bool* d_state, unsigned long tt){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idy*LX + idx;
		float r1, r2;
		PhiloxRandomPair(index, tt, &r1, &r2);
		d_sigma[index] = DeviceBoxMuller(r1, r2)/2.;
		d_state[index] = (fabs(d_sigma[index])>1);
	}
}

__global__ void Kernel_InitRandomExponential(REAL* d_sigma_threshold, unsigned long tt){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idy*(LX/2) + idx;
		int index1 = 2*index;
		int index2 = 2*index+1;
		float r1, r2;
		PhiloxRandomPair(index, tt, &r1, &r2);
		d_sigma_threshold[index1] = -log(r1);
		d_sigma_threshold[index2] = -log(r2);
	}
}

__global__ void Kernel_InitRandomBouchaud(REAL* d_sigma_threshold, unsigned long tt){
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idy*(LX/2) + idx;
		int index1 = 2*index;
		int index2 = 2*index+1;
		float r1, r2;
		PhiloxRandomPair(index, tt, &r1, &r2);
//		d_sigma_threshold[index1] = sqrt(2*(-log(r1)));
//		d_sigma_threshold[index2] = sqrt(2*(-log(r2)));
		d_sigma_threshold[index1] = 2*sqrt((E_MIN - log(r1)/LAMBDA));
		d_sigma_threshold[index2] = 2*sqrt((E_MIN - log(r2)/LAMBDA));
	}
}


__global__ void Kernel_SetPropagator(REAL *g, int nx, int ny){
	const unsigned int i = blockIdx.x*blockDim.x+threadIdx.x;
	const unsigned int j = blockIdx.y*blockDim.y+threadIdx.y;
	if ( i < nx && j < ny){
			int iprime = (i <= LX/2) ? i : i - LX;
			REAL qx=2.0*M_PI*iprime/(REAL)LX;
			REAL qy=2.0*M_PI*j/(REAL)LY;
			REAL q2=qx*qx+qy*qy;

			if(i!=0 || j!=0) g[i*ny+j] = -4*qx*qx*qy*qy/(q2*q2);
			if (i==0 && j==0) g[i*ny+j] = -1.0;
	}
}


//////////-------------------------Update Kernels------------------///////////////////////////

#ifdef TIMEFIX
__global__ void Kernel_UpdateStateVariablesRandomThreshold(REAL* d_sigma, REAL* d_sigma_threshold, REAL* d_accstrain, \
						unsigned int* d_clock, bool* d_state, unsigned long long int time)
#else
__global__ void Kernel_UpdateStateVariablesRandomThreshold(REAL* d_sigma, REAL* d_sigma_threshold, REAL* d_accstrain, \
						bool* d_state, bool* d_state_aux, unsigned long long int time)
#endif
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	unsigned int idx = blockIdx.x*blockDim.x+threadIdx.x;
	unsigned int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		unsigned int index = idy*(LX/2) + idx;
		bool actualstate;
		REAL actualsigma, actualthreshold, actualaccstrain;
		float r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);

		for (int j=0;j<2;j++)
		{
			unsigned int index2 = 2*index+j;
			REAL rannum = r1*(j==0)+r2*(j==1);
			actualstate = d_state[index2];
			actualsigma = d_sigma[index2];
			actualthreshold = d_sigma_threshold[index2];
			actualaccstrain = d_accstrain[index2];
#ifdef TIMEFIX
			unsigned int actualclock=d_clock[index2];
#endif
			if(actualstate==0 && (fabs(actualsigma)>actualthreshold) ){
				d_state[index2]=1;
				d_state_aux[index2]=1;
				d_sigma_threshold[index2]=2*sqrt((E_MIN - log(rannum)/LAMBDA));
			}
#ifdef TIMEFIX
			if(actualstate==1){
				if((actualaccstrain>STRAIN_THRESHOLD) || (actualclock>=CLOCK_THRESHOLD)){
					d_state[index2]=0;
					d_accstrain[index2]=0;
					d_clock[index2]=0;
				}else{
					d_clock[index2]+=1;
				}
			}
#else
			if(actualstate==1 && (actualaccstrain>STRAIN_THRESHOLD) ){
				d_state[index2]=0;
				d_accstrain[index2]=0;
			}
#endif
		}

	}
}

__global__ void Kernel_UpdateStateVariables(const REAL* d_sigma, bool* d_state, int time)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX/2 && idy < LY){
		int index = idy*(LX/2) + idx;
		int index1 = 2*index;
		int index2 = 2*index+1;
		const bool actualstate1 = d_state[index1];
		const bool actualstate2 = d_state[index2];
		float r1, r2;
		PhiloxRandomPair(index, time, &r1, &r2);
		d_state[index1]= ((actualstate1==0 && fabs(d_sigma[index1])>=STRESS_THRESHOLD && r1<TIME_STEP/TAU_PLAST)|| \
		(actualstate1==1 && r1>TIME_STEP/TAU_ELAST));
		d_state[index2]= ((actualstate2==0 && fabs(d_sigma[index2])>=STRESS_THRESHOLD && r2<TIME_STEP/TAU_PLAST)|| \
		(actualstate2==1 && r2>TIME_STEP/TAU_ELAST));
	}
}

// Pointwise multiplication
static __global__ void Kernel_PointwiseMul(const REAL *a, const bool *b, REAL* c)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idy*LX + idx;
		c[index] = a[index] * b[index];
	}
}

// COMPLEX pointwise multiplication
static __global__ void Kernel_ComplexPointwiseMulAndScale(const COMPLEX* a, const REAL* b, COMPLEX* c, REAL scale)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < (LY/2+1)){
		int index = idy*LX + idx;
		c[index].x = scale*(a[index].x * b[index]);
		c[index].y = scale*(a[index].y * b[index]);
	}
} 

static __global__ void Kernel_EulerIntegrationStep(REAL *d_sigma, const REAL *d_dsigma_r, const bool *d_state, REAL *d_accstrain, REAL shear_rate, REAL dt)
{
	// compute idx and idy, the location of the element in the original LX*LY array 
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int idy = blockIdx.y*blockDim.y+threadIdx.y;
	if ( idx < LX && idy < LY){
		int index = idy*LX + idx;

		d_accstrain[index] += d_state[index]*fabs( shear_rate + d_dsigma_r[index] + d_sigma[index] )*dt;

		//There is a factor of 2 missing here (multipling d_dsigma_r), that would cancell with the factor of 1/2 \
		that is missing in the calculus of d_dsigma_r as well. Everyone happy.
		d_sigma[index]+=(shear_rate+d_dsigma_r[index])*dt;

	}
}


