This code was originally implemented for: "Driving rate dependence of avalanche statistics and shapes at the yielding transition",
C. Liu, E.E. Ferrero, F. Puosi, J.-L. Barrat and K. Martens
http://arxiv.org/abs/1506.08161
Phys. Rev. Lett. 116, 065501 (2016)
http: http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.116.065501
DOI: http://dx.doi.org/10.1103/PhysRevLett.116.065501

The code is freely available under GNU GPL v3.  

A Supplemental Material was published together with the paper, describing the code  
http://www-liphy.ujf-grenoble.fr/pagesperso/martens/documents/liu2015-sm.pdf
http://journals.aps.org/prl/supplemental/10.1103/PhysRevLett.116.065501
You can find it also in this repository as SM.pdf  

Please cite when appropriate.

